

const { readTheFile, createNewFileAndconvertContentToUpperCase, readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile, readNewFileAndSortTheContentWriteItIntoNewFile,
    readContentsAndDeleteAllNewFiles } = require("../problem2.js");


readTheFile(
    function () {
        createNewFileAndconvertContentToUpperCase(() => {
            readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile(() => {
                readNewFileAndSortTheContentWriteItIntoNewFile(() => {
                    readContentsAndDeleteAllNewFiles(() => {
                        if(error) {
                            console.log(error);
                        }
                        else {
                            console.log("Successfully deleted the new files.");
                        }
                    });
                });
            });
        });
    }
);




