/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/




const fs = require('fs');

function readTheFile(createNewFileAndconvertContentToUpperCase) {
    fs.readFile('../lipsum.txt', 'utf8', (err, data) => {
        if (err) throw err;
        console.log(data);
    });
    createNewFileAndconvertContentToUpperCase();
}

function readContentsAndDeleteAllNewFiles () {
    fs.readFile('../filenames.txt', 'utf8', (err, data) => {
        if (err) throw err;
        let dataIntoLowerCase = data.toLowerCase();
        let dataSplit = dataIntoLowerCase.split('.');
        // let dataSorted = dataSplit.sort();
        fs.writeFile('../sentences.txt', dataSplit.join('\n'), (err) => {
            if (err) throw err;
            console.log('New file created and added sentences data into it');
        });
        fs.unlink('../newFileNames.txt', (err) => {
            if (err) throw err;
            console.log('All new files deleted');
        });
    });
    
}

function readNewFileAndSortTheContentWriteItIntoNewFile(readContentsAndDeleteAllNewFiles) {
    fs.readFile('../sentences.txt', 'utf8', (err, data) => {
        if (err) throw err;
        let dataSorted = data.split("\n").sort();
        console.log(dataSorted, "data sorted");
        fs.writeFile('../newFileNames.txt', JSON.stringify(dataSorted), (err) => {
            if (err) throw err;
            console.log('New file created and sorted data');
        });
    });
    readContentsAndDeleteAllNewFiles();
}


function readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile(readNewFileAndSortTheContentWriteItIntoNewFile) {
    fs.readFile('../filenames.txt', 'utf8', (err, data) => {
        if (err) throw err;
        let dataIntoLowerCase = data.toLowerCase();
        let dataSplit = dataIntoLowerCase.split('.');
        // let dataSorted = dataSplit.sort();
        fs.writeFile('../sentences.txt', dataSplit.join('\n'), (err) => {
            if (err) throw err;
            console.log('New file created and added sentences data into it');
        });
    });
    readNewFileAndSortTheContentWriteItIntoNewFile();
}



function createNewFileAndconvertContentToUpperCase(readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile) {
    fs.readFile('../lipsum.txt', 'utf8', (err, data) => {
        if (err) throw err;
        let dataIntoUpperCase = data.toUpperCase();
        fs.writeFile('../filenames.txt', dataIntoUpperCase, (err) => {
            if (err) throw err;
            console.log('New file created and added lipsum data into it');
        });
    });
    readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile();
}
module.exports = { readTheFile, createNewFileAndconvertContentToUpperCase, readNewFileAndSplitContentsIntoSentencesWriteItIntoNewFile, readNewFileAndSortTheContentWriteItIntoNewFile, readContentsAndDeleteAllNewFiles};