module.exports = createAndDeleteJSONFiles;

// Import fs (File system) Module
const fs = require("fs");
// Function to create a directory of random JSON files
function createAndDeleteJSONFiles(directoryPath, maximumFiles) {
    fs.mkdir(directoryPath, { recursive: true }, (error1) => {
        if (error1) {
            console.log(error1);
        }
        else {
            console.log("Successful created a directory")
        }
    });

    let numberOfJSONFiles = Math.floor(Math.random() * (maximumFiles - 1));
    function deleteFiles(fileNumber) {
        if (fileNumber < numberOfJSONFiles) {
            fs.unlink(directoryPath + `/file_${fileNumber + 1}.json`, function (error2) {
                if (error2) {
                    console.log("Error deleting file", error2);
                }
                else {
                    console.log(`file no. ${fileNumber + 1} deleted successfully.`);
                    deleteFiles(fileNumber + 1);
                }
            });
        }
    }
    function writeFiles(fileNumber) {
        if (fileNumber < numberOfJSONFiles) {
            let fileData = { file_name: `file_${fileNumber + 1}.json`, data: `This is a random JSON File no. ${fileNumber} created by asynchronous functions of fs modules.` };
            
            fs.writeFile(directoryPath + `/file_${fileNumber + 1}.json`, JSON.stringify(fileData), function (error2, data) {
                if (error2) {
                    console.log("Error writing file", error2);
                }
                else {
                    console.log(`File no. ${fileNumber + 1} created successfully.`);
                    writeFiles(fileNumber + 1);
                }
            });
        }
        else {
            deleteFiles(0);
        }
    }
    writeFiles(0);
}

